// Calculates payroll
function payroll_calc() {
	// Inputs
	let name = document.getElementById("name_of_person").value;
	let rate = parseFloat(document.getElementById("rate_of_pay").value);
	let hours = parseFloat(document.getElementById("hours_worked").value);

	// Outputs
	let output_name = document.getElementById("output_name");
	let output_rate = document.getElementById("output_rate");
	let output_hours = document.getElementById("output_hours");
	let output_pay = document.getElementById("output_pay");

	let pay;

	// Caculate Pay (40hrs = overtime)
	/// Is overtime?
	if (hours > 40) {
		// overtime
		pay = (rate * (hours + 4)).toFixed(2);
	}
	else {
		// Not overtime
		pay = (rate * hours).toFixed(2);
	}

	// Send Outputs
	output_name.innerHTML = name;
	output_rate.innerHTML = "$" + parseFloat(rate).toFixed(2) + " per hour";
	output_hours.innerHTML = hours;
	output_pay.innerHTML = "$" + pay;
}


