function calculate(){
	let price = parseFloat(document.getElementById("price").value);
	let service = parseInt(document.getElementById("service").value);
	let output = document.getElementById("output");

	// calculation
	let tipPercent = service / 100;

	let tipAmount = price * tipPercent;

	// parse = read the string and put a float number on it then do toFixed. also parseInt()
	let total = parseFloat(price + tipAmount).toFixed(2);

	// to round --> add one half of place you want then truncate 
	output.innerHTML = "Maybe tip $" + tipAmount.toFixed(2) + " for a total of $" + total + ".";
}
