# Description
<p>This website is for my SDEV 153 class. In this class I'm learning how to build websites. Code
in HTML, CSS, and JavaScript. Also we did research into setting up a free hosting domain.</p>
<br>
<p>This project I'm using GitLab Pages to host my website with. It is completely free to host
a website with GitLab.</p>
<br>
<p>Here is a link how to create a fully working website with GitLab Pages really easily.</p>
<br>
[Create a Website with GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_new_project_template.html)

# Link to Website
[https://dbdistro.gitlab.io/sdev153](https://dbdistro.gitlab.io/sdev153)


# Contact Information
PRIVATE: <dbDistro23@duck.com>
<br>
SCHOOL: <mbutler147@ivytech.edu>
